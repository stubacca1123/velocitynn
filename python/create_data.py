#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create the FP32 dataset for RTM
"""

from urllib.request import urlretrieve
import gzip
import os
import numpy as np

from scipy import interpolate as intp

import math
import subprocess
import sys
import time
import copy

# import segyio
import hdf5storage as h5mat
import h5py as h5
from shutil import copyfile

"""
    _______________________Constants for inversion_____________________________
"""
f0 = 25  # Center frequency of the source
dsrc = 60  # Put a source every dsrc grid cell
dg = 6  # Put a receiver every dg grid cell
seisout = 2  # Output pressure
nab = 128  # Number of cell in absorbing layer

filepath = os.getcwd() + '/'

"""
_______________________Download the velocity model_____________________________
"""

# file = open('../Fig5_inversion/madagascar/vel.asc', 'r')
# vel = [float(el) for el in file]
#
# vp = np.transpose(np.reshape(np.array(vel), [2301, 751]))

"""
    ____________________________Add absorbing layer_____________________________
"""

(NZ, NX) = vp.shape
NZ = int(NZ / 2) * 2
NX = int(NX / 2) * 2
vp = vp[:NZ, :NX]

vp2 = np.zeros([vp.shape[0] + 2 * nab + 80, vp.shape[1] + 2 * nab])
vp2[nab + 80:-nab, nab:-nab] = vp
vp2[0:nab + 80, :] = 1500
for ii in range(vp2.shape[1]):
    vp2[-nab:, ii] = vp2[-nab - 1, ii]
for ii in range(vp2.shape[0]):
    vp2[ii, 0:nab] = vp2[ii, nab]
    vp2[ii, -nab:] = vp2[ii, -nab - 1]
vp = vp2
rho = vp * 0 + 2000
vs = vp * 0

# """
# _____________________________Plot models______________________________________
# """
# import matplotlib.pyplot as plt
# from mpl_toolkits.axes_grid1 import make_axes_locatable
# fig, ax = plt.subplots()
# im = ax.imshow(vp, interpolation='bilinear')
#
# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.05)
#
# plt.colorbar(im, cax=cax)
#
# plt.show()


"""
_____________________Simulation constants input file___________________________
"""
csts = {}
csts['N'] = np.array([vp.shape[0], vp.shape[1]])
csts['NX'] = vp.shape[1]  # Grid size in X
csts['NY'] = 1  # Grid size in Y (set to 1 for 2D)
csts['NZ'] = vp.shape[0]  # Grid size in Z
csts['ND'] = 2  # Flag for dimension. 3: 3D, 2: 2D P-SV,  21: 2D SH
csts['dh'] = 4  # Grid spatial spacing

csts['dt'] = 6 * csts['dh'] / (7 * np.sqrt(2) * np.max(vp)) * 0.85  # Time step
csts['NT'] = int(4 / csts['dt'])  # Number of time steps
csts['freesurf'] = 0  # Include a free surface at z=0: 0: no, 1: yes
csts['FDORDER'] = 4  # Order of the finite difference stencil.
csts['MAXRELERROR'] = 0  # Set to 1
csts['f0'] = f0  # Central frequency
csts['L'] = 0
csts['FL'] = np.array(5)

csts['src_pos'] = np.empty((5, 0))  # Position of each shots.
csts['rec_pos'] = np.empty((8, 0))  # Position of the receivers.
csts['src'] = np.empty((csts['NT'], 0))  # Source signals. NTxnumber of sources

csts['abs_type'] = 2  # Absorbing boundary type: 1: CPML, 2: Absorbing layer
csts['VPPML'] = 3000  # Vp velocity near CPML boundary
csts['NPOWER'] = 2  # Exponent used in CMPL frame update
csts['FPML'] = f0  # Dominant frequency of the wavefield
csts['K_MAX_CPML'] = 2  # Coeffienc involved in CPML
csts['nab'] = nab  # Width in grid points of the absorbing layer
csts['abpc'] = 6  # Exponential decay of the absorbing layer of Cerjan et. al.
csts['pref_device_type'] = 4  # Type of processor used: 2: CPU, 4: GPU
csts['nmax_dev'] = 1  # Maximum number of devices that can be used
csts['no_use_GPUs'] = np.empty((1, 0))  # GPUs not to use
csts['MPI_NPROC_SHOT'] = 1  # Maximum number of MPI process (nodes) per shot
csts['back_prop_type'] = 1  # Type of gradient calculation: 1: backpropagation
csts['par_type'] = 0  # Type of paretrization: 0:(rho,vp,vs,taup,taus)
csts['tmax'] = csts['NT'] * csts['dt']  # Maximum time to compute gradient
csts['tmin'] = 0.0  # Minimum time for which the gradient is to be computed
csts['scalerms'] = 0  # Scale each modeled and recorded traces according rms
csts['scalermsnorm'] = 0  # Scale each modeled and recorded traces
csts['scaleshot'] = 0  # Scale all of the traces in each shot shot rms
csts['fmin'] = 0  # Maximum frequency for the gradient computation
csts['fmax'] = 0  # Minimum frequency for the gradient computation
csts['mute'] = None  # Muting matrix 5xnumber of traces.
csts['weight'] = None  # NTxnumber of geophones or 1x number of geophones.
csts['gradout'] = 0  # Output gradient 1:yes, 0: no
csts['HOUT'] = 1  # Output gradient 1:yes, 0: no
csts['gradsrcout'] = 0  # Output source gradient 1:yes, 0: no
csts['seisout'] = 2  # Output seismograms 1:velocities, 2: pressure
csts['resout'] = 0  # Output residuals 1:yes, 0: no
csts['rmsout'] = 0  # Output rms value 1:yes, 0: no
csts['MOVOUT'] = 0  # Output movie 1:yes, 0: no
csts['restype'] = 0  # Type of costfunction 0: raw seismic 1: RTM
csts['FP16'] = 1  # Create data in FP32

"""
_________________________Sources and receivers_________________________________
"""

tmin = -2 / csts['f0']
t = np.zeros((csts['NT'], 1))
t[:, 0] = tmin + np.linspace(0, csts['NT'] * csts['dt'], csts['NT'])
pf = math.pow(math.pi, 2) * math.pow(csts['f0'], 2)
ricker = np.multiply((1.0 - 2.0 * pf * np.power(t, 2)),
                     np.exp(-pf * np.power(t, 2)))

gx = np.zeros(len(range(csts['nab'] + 5, csts['NX'] - csts['nab'] - 5, dg)))
gz = np.zeros(len(range(csts['nab'] + 5, csts['NX'] - csts['nab'] - 5, dg)))
n = 0
for jj in range(csts['nab'] + 5, csts['NX'] - csts['nab'] - 5, dg):
    gx[n] = (jj) * csts['dh']
    gz[n] = (csts['nab'] + 5) * csts['dh']
    n += 1

for ii in range(csts['nab'] + 5, csts['NX'] - csts['nab'] - 5, dsrc):
    idsrc = csts['src_pos'].shape[1]
    toappend = np.zeros((5, 1))
    toappend[0, :] = (ii) * csts['dh']
    toappend[1, :] = 0
    toappend[2, :] = (csts['nab'] + 5) * csts['dh']
    toappend[3, :] = idsrc
    toappend[4, :] = 100
    csts['src_pos'] = np.append(csts['src_pos'], toappend, axis=1)
    csts['src'] = np.append(csts['src'], ricker, axis=1)

    toappend = np.stack([gx,
                         gx * 0,
                         gz,
                         gz * 0 + idsrc,
                         np.arange(0, len(gx)) + csts['rec_pos'].shape[1] + 1,
                         gx * 0 + 2,
                         gx * 0,
                         gx * 0], 0)
    csts['rec_pos'] = np.append(csts['rec_pos'], toappend, axis=1)

"""
___________________________Do the modeling__________________________________
"""

file = filepath + "marmfp32"
filenames = {}
filenames['model'] = file + "_model.mat"  # File containing the model pareters
# File containing the modeling constants
filenames['csts'] = file + "_csts.mat"
filenames['din'] = file + "_din.mat"  # File containing the recorded data
# File containing the seismograms output
filenames['dout'] = file + "_dout.mat"
filenames['gout'] = file + "_gout.mat"  # File containing the gradient ouput
filenames['rms'] = file + "_rms.mat"  # File containing the rms ouput
filenames['movout'] = file + "_movie.mat"  # File containing the movie ouput
# File containing the recorded data
filenames['dmute'] = file + "_din_mute.mat"

h5mat.savemat(filenames['csts'],
              csts,
              appendmat=False,
              format='7.3',
              store_python_metadata=True,
              truncate_existing=True)
h5mat.savemat(filenames['model'],
              {'vp': vp, 'vs': vs, 'rho': rho},
              appendmat=False,
              format='7.3',
              store_python_metadata=True,
              truncate_existing=True)

cmdlaunch = ('mpirun -np 1 SeisCL_MPI '
             + file + ' > '
             + filepath + 'out 2>'
             + filepath + 'err')
print(cmdlaunch)
pipes = subprocess.Popen(cmdlaunch,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE, shell=True)
while (pipes.poll() is None):
    time.sleep(1)
sys.stdout.write('Forward calculation completed \n')
sys.stdout.flush()

copyfile(filenames['dout'], filenames['din'])
datain = h5.File(filenames['din'], 'a')
datain['p'] = datain['pout']
datain.close()
