#!/usr/bin/env python3
"""
python script for generating psuedo random dipping 2d model with dip
"""

__author__ = "Stuart Farris"
__version__ = "0.1.0"
__license__ = "cs230 project"

import numpy as np
import math

def rand_2d_dipping(nx=600,nz=200,dx=10,dz=10,vmin=1500,vmax=4500,dvmin=-600,dvmax=600,minthick=500,maxthick=1000,maxdip=20): 
    """
    dz = dx sampling interval in z and x direction
    nz = number of indices in z direction 
    nx = number of indices in x direction
    vmin = min velocity value in m/sec
    vmax = max velocity value in m/sec
    dvmin = minimum d_velocity in m/sec. min change in velocity between layers.
    dvmax = maximum d_velocity in m/sec. max change in velocity between layers
    maxdip = max amount on dip in either direction
    minthick = min layer thickness (in meter float values not indices)
    maxthick = max layer thickness (in meter float values not indices)

    """
    #pick random dip
    dip = (np.random.random_sample()-0.5)*2*maxdip
    
    #calculate padding so we have layers throughout model even with dip
    pad_z = int(dx*nx*math.tan(math.fabs(dip)/360*2*math.pi)/dz)
    nz_orig = nz
    nz = nz+2*pad_z
    
    # Random draw of nz velocities (velocities as factor of 10
    dvel = (np.random.randint(dvmin/10,dvmax/10,nz))*10
    vel = np.zeros(nz)
    vel[0] = vmin
    for i in np.arange(nz)-1:
        vel[i+1] = ((vmax-vmin)/nz*i+vmin+vel[i])/2 + dvel[i]
        
    # Make 1D layer cake earth model blocky with psuedo-random thickness interval
    minthick=minthick/dz
    maxthick=maxthick/dz
    last_index = 0
    vel_block = np.zeros(vel.shape)
    while True:
        thick_pixels = int(np.random.randint(minthick,maxthick, size=1)/dz)
        if last_index+thick_pixels >= nz:
            vel_block[last_index:] = np.sum(vel[last_index:])/(nz-last_index)
            break
        else:
            vel_block[last_index:last_index+thick_pixels] = np.sum(vel[last_index:last_index+thick_pixels])/thick_pixels
        last_index = last_index + thick_pixels
    
    # make 2D layer cake from blocky layer cake with some dip
    
    vel_2D = np.zeros((nz,nx))
    #print vel_2D.shape
    shift_constant = math.tan(dip/360*2*math.pi)/dz
    #print dip
    for i in np.arange(nx):
        cur_dx = dx*i #float shift in x direction
        pixel_shift = int(cur_dx*shift_constant)
        if pixel_shift >= 0:
            vel_2D[pixel_shift:,i]=vel_block[:vel_block.shape[0]-pixel_shift]
        else:
            #vel_2D[:,i]=vel_block
            vel_2D[:pixel_shift,i]=vel_block[-pixel_shift:]
            
    return vel_2D[pad_z:pad_z+nz_orig,:]-np.mean(vel_2D[pad_z:pad_z+nz_orig,:])+(vmin+vmax)/2
    #return vel_2D[pad_z:pad_z+nz_orig,:]/1000


if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()

