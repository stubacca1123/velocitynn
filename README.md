# velocityNN
# INTRO
This is the repo for the CS230 - Deep Learning group project. Participants are:
* Vishal Das
* Abhishek Tapadar
* Stuart Farris

# SUMMARY
This project will (hopefully)
1. Generate CMP training data by:
   1. Generate psuedo-random interval velocity models
   2. forward prop the wave equation in said models to get shot gathers
   3. sort shot gathers into common midpoint gathers (CMP)s
2. Generate training labels by:
   1. Convert interval velocity models from 1. into RMS velocity models.
3. Train a NN to predict 2D VRMS from CMPs 

